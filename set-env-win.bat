@echo off

rem Application
setx CLUSTERRA_APP_HOME ~/pmbok-app
setx CLUSTERRA_APP_NAME pmbok

rem Session and security
setx CLUSTERRA_USER_SESSION_TIMEOUT 3600
setx CLUSTERRA_ENCRYPTION_KEY 1a2b3c4d

rem Database connection
setx CLUSTERRA_DB_SERVER_URL jdbc:postgresql://localhost:5432
setx CLUSTERRA_DB_DRIVER org.postgresql.Driver
setx CLUSTERRA_DB_USER postgres
setx CLUSTERRA_DB_PASSWORD secret

rem email
setx CLUSTERRA_MAIL_SMTP_HOST smtp.timeweb.ru
setx CLUSTERRA_MAIL_SMTP_PORT 465
setx CLUSTERRA_MAIL_FROM support@clusterra.com
setx CLUSTERRA_MAIL_USERNAME support@clusterra.com
setx CLUSTERRA_MAIL_PASSWORD secret
setx CLUSTERRA_MAIL_TO_OPS support@clusterra.com


rem Build
setx ORG_GRADLE_PROJECT_signing_keyId=_XXFFGGFF_
setx ORG_GRADLE_PROJECT_signing_password=_secret_
setx ORG_GRADLE_PROJECT_signing_secretKeyRingFile=_/Users/kepkap/.gnupg/secring.gpg_
setx GRADLE_OPTS=-Dorg.gradle.daemon=true



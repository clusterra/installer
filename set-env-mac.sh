#!/bin/sh

#Application
export CLUSTERRA_APP_HOME=~/pmbok-app
export CLUSTERRA_APP_NAME=pmbok

#Session and security
export CLUSTERRA_USER_SESSION_TIMEOUT=3600
export CLUSTERRA_ENCRYPTION_KEY=1a2b3c4d

#Database connection
export CLUSTERRA_DB_SERVER_URL=jdbc:postgresql://localhost:5432
export CLUSTERRA_DB_DRIVER=org.postgresql.Driver
export CLUSTERRA_DB_USER=postgres
export CLUSTERRA_DB_PASSWORD=secret

#Email
export CLUSTERRA_MAIL_SMTP_HOST=smtp.timeweb.ru
export CLUSTERRA_MAIL_SMTP_PORT=465
export CLUSTERRA_MAIL_FROM=support@clusterra.com
export CLUSTERRA_MAIL_USERNAME=support@clusterra.com
export CLUSTERRA_MAIL_PASSWORD=secret
export CLUSTERRA_MAIL_TO_OPS=support@clusterra.com

#Build
export ORG_GRADLE_PROJECT_signing_keyId=_XXFFGGFF_
export ORG_GRADLE_PROJECT_signing_password=_secret_
export ORG_GRADLE_PROJECT_signing_secretKeyRingFile=_/Users/kepkap/.gnupg/secring.gpg_
export GRADLE_OPTS=-Dorg.gradle.daemon=true


#Optional, better cmd prompt in git folders.
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\e[0;33m\u@\h \e[0;37m\w \e[0;32m \$(parse_git_branch)\[\033[00m\] $ "


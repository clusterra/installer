# Clusterra PMBOK Installation

Clusterra uses [Gradle] for installation.
`gradle.properties` has property `pmBokVersion` specifying release verion to install. 

For instance, `pmBokVersion=1.0.14.RELEASE`, means artifact     
`com.clusterra:clusterra-pmbok-server-runner:1.0.14.RELEASE`.    

This *Main* artifact must be available via one of repositories    
[![The Central Repository](https://img.shields.io/badge/Artifacts-The%20Central%20Repository-orange.svg?style=flat-square)][iam-artifacts-mvn]
[![Sonatype OSS](https://img.shields.io/badge/Artifacts-Sonatype%20OSS-orange.svg?style=flat-square)][iam-artifacts-sonatype]     

Usually, this property gets updated right after dev team uploaded next release to repositories.
All dependencies will be downloaded transitively.     

Praise to Open Source Software(OSS)! :heart::heart::heart:
 
## 1. Back-end

### 1.1. Clone installer repo

`https://bitbucket.org/clusterra/installer.git`   

It may be required to `unset SSH_ASKPASS` on some linux environments, to switch asking for password from command prompt.

### 1.2. Set environment variables
Use the best way to set env vars for the platform. Below files have all required variables for convenience, for linux based servers consider using ~/.bash_profile.

* `set-env-win.bat` 
* `set-env-mac.sh`

Application will start with reasonable defaults.   
If required, parameters can be adjusted using the following env variables   

#### 1.2.1. Application
`CLUSTERRA_APP_HOME=_~/pmbok-app_`    
`CLUSTERRA_APP_NAME=_pmbok_`    

#### 1.2.2. Session and security
`CLUSTERRA_USER_SESSION_TIMEOUT=_3600_`    
`CLUSTERRA_ENCRYPTION_KEY=_1a2b3c4d_` 

#### 1.2.3. Database connection
`CLUSTERRA_DB_SERVER_URL=_jdbc:postgresql://localhost:5432/_`    
`CLUSTERRA_DB_DRIVER=_org.postgresql.Driver_`    
`CLUSTERRA_DB_USER=_postgres_`    
`CLUSTERRA_DB_PASSWORD=_secret_`    

#### 1.2.4. Email
`CLUSTERRA_MAIL_SMTP_HOST=_smtp.your-server.com_`    
`CLUSTERRA_MAIL_SMTP_PORT=_465_`    
`CLUSTERRA_MAIL_FROM=_email_`    
`CLUSTERRA_MAIL_USERNAME=_support@your-server.com_`    
`CLUSTERRA_MAIL_PASSWORD=_secret_`    
`CLUSTERRA_MAIL_TO_OPS=_support@your-server.com_`    


#### 1.2.5. Build properties
Used by build tool for signing jars. Required only for publishing artifacts to repositories.

`ORG_GRADLE_PROJECT_signing_keyId=_XXFFGGFF_ `    
`ORG_GRADLE_PROJECT_signing_password=_secret_ `    
`ORG_GRADLE_PROJECT_signing_secretKeyRingFile=_/Users/kepkap/.gnupg/secring.gpg_ `    
`GRADLE_OPTS=-Dorg.gradle.daemon=true `    
   
Check [environment set-up][env-set-up] out.

### 1.3. Run installation process

Navigate to repository root dir and execute one of the following tasks:

* `gradlew _install` (installs application from artifactory, creates db, starts windows service)
* `gradlew dropdb` (drops database instance)
* `gradlew createdb` (creates database instance)
* `gradlew _clean` (clean application home dir)

Application is [wrapped][tanuki-wrapper]. Installation will produce ```CLUSTERRA_APP_HOME/bin/clusterra-app``` executable. Run it to get application args printed-out and follow instructions.

## 2. Front-end
todo



[env-set-up]: https://bitbucket.org/clusterra/docs/wiki/Environment%20set-up
[tanuki-wrapper]: http://wrapper.tanukisoftware.com/doc/english/introduction.html
[Gradle]: http://gradle.org
[iam-artifacts-mvn]: http://search.maven.org/#search%7Cga%7C1%7Cg%3A%22com.clusterra%22
[iam-artifacts-sonatype]: https://oss.sonatype.org/#nexus-search;quick~com.clusterra